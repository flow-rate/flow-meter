# Flow Meter

The backend service for the Flow Rate project.

Run automated performance "flow" tests on SPA web sites.

Powered by Puppeteer running on AWS Lambda.