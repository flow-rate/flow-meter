import { APIGatewayProxyHandler } from 'aws-lambda';
import { runTest } from './run-test';

export const flowRunner: APIGatewayProxyHandler = async (event) => {
  // const { url } = event.queryStringParameters;
  const testRun = JSON.parse(event.body);

  const result = await runTest(testRun).then(testResult => {
    return {
      statusCode: 200,
      body: JSON.stringify(testResult)
    };
  }).catch(err => {
    return {
      statusCode: 400,
      body: err
    };
  });

  return result;
};
