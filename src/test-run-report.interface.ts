import { TraceReport } from './trace/report.interface';

export interface TestRunReport {
  reportName: string;
  stepReports: Array<TestRunStepReport>;
}

export interface TestRunStepReport {
  flowReport: TraceReport;
}
