import Jimp from 'jimp';
import { Trace, TraceEvent } from './trace.interface';
import { VisualUpdateEvent } from './report.interface';

const VISUAL_UPDATE_THRESHOLD = 0.1;

export async function getVisualEvents(trace: Trace): Promise<Array<VisualUpdateEvent>> {
    const screenshotsTraceEvents = trace.traceEvents.filter(event => event.name === 'Screenshot');
    const renderEvents = trace.traceEvents.filter(event => event.name === 'CompositeLayers' && event.ph === 'E');

    const images = await Promise.all(
      screenshotsTraceEvents.map(snapshot => Buffer.from(snapshot.args.snapshot, 'base64'))
      .map(imageBuffer => Jimp.read(Buffer.from(imageBuffer)))).catch(err => {
        console.error('error parsing image', err);
        throw err;
      });

    const visualUpdates: Array<{first: TraceEvent, second: TraceEvent, diff: number}> = images
      .reduce((updates, image, index, allImages) => {
        if (index === 0) {
          return updates;
        }
        const diff = Jimp.diff(image, allImages[index - 1]);
        if (diff.percent > VISUAL_UPDATE_THRESHOLD) {
          updates.push({
            first: screenshotsTraceEvents[index - 1],
            second: screenshotsTraceEvents[index],
            diff: diff.percent
          })
        }
        return updates;
      }, []);

    return Promise.resolve(visualUpdates.map(visualUpdate => {
      const rendersInUpdatePeriod = renderEvents.filter(event => event.ts > visualUpdate.first.ts && event.ts < visualUpdate.second.ts);

      return {
        type: 'visualUpdate',
        timestamp: rendersInUpdatePeriod[rendersInUpdatePeriod.length - 1].ts,
        beforeSnapshot: visualUpdate.first.args.snapshot,
        afterSnapshot: visualUpdate.second.args.snapshot,
        difference: visualUpdate.diff
      } as VisualUpdateEvent;
    }));
}