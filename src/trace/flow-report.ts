import { getVisualEvents } from './visual-events';
import { Trace } from './trace.interface';
import { FlowEvent, TraceReport, UserInteractionEvent, VisualUpdateEvent } from './report.interface';
import { getUserInteraction } from './user-interaction';

export async function getFlowEvents(trace: Trace): Promise<Array<FlowEvent>> {
  const visualEvents = await getVisualEvents(trace).catch(err => {
    console.error('error getting visual events', err);
    throw err;
  });
  const interactionEvent = getUserInteraction(trace);

  const events = [
    interactionEvent,
    ...visualEvents
  ];
  console.log('event', events);
  return Promise.resolve(events);
}

export async function getFlowReport(trace: Trace): Promise<TraceReport> {
  console.log('flowreport');
  const events = await getFlowEvents(trace).catch(err => {
    console.log('error getting flow events', err);
    throw err;
  });

  const entryEvent = events[0] as UserInteractionEvent;
  const firstPaintEvent = events[1] as VisualUpdateEvent;
  const finalPaint = events[events.length - 1] as VisualUpdateEvent;

  const report: TraceReport = {
    events,
    entryEvent,
    firstPaint: firstPaintEvent ? {
      event: firstPaintEvent,
      deltaTime: (firstPaintEvent.timestamp - entryEvent.timestamp)/1000
    } : null,
    finalPaint: finalPaint ? {
      event: finalPaint,
      deltaTime: (finalPaint.timestamp - entryEvent.timestamp)/1000
    } : null
  };

  return Promise.resolve(report);
}