import { UserInteractionEvent } from "./report.interface";
import { Trace } from "./trace.interface";

export function getUserInteraction(trace: Trace): UserInteractionEvent {
  const clickEvent = trace.traceEvents.find(event => event.name === 'EventDispatch' && event.args.data.type === 'click');

  return clickEvent && {
    type: 'userInteraction',
    interactionType: 'click',
    timestamp: clickEvent.ts
  };
}