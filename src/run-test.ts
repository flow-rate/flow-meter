import { TestRun, TestRunStep } from './test-run.interface';
import { TestRunReport, TestRunStepReport } from './test-run-report.interface';

import { connect, Page } from 'puppeteer';
import { getChrome } from './chrome-script';
import { getFlowReport } from './trace/flow-report';
import { Trace } from '.';

export async function runTest(test: TestRun): Promise<{ report: TestRunReport, traces: Array<Trace> }> {
  console.log('Running report', test.reportName);
  const isValid = validateTestRun(test);
  if (!isValid) {
    throw new Error('Test config is invalid');
  }

  const chrome = await getChrome().catch(err => {
    throw err;
  });
  const browser = await connect({
    browserWSEndpoint: chrome.endpoint
  }).catch(err => {
    throw err;
  });
  const page = await browser.newPage().catch(err => {
    throw err;
  });
  await page.goto(test.entryUrl, { waitUntil: 'networkidle0' }).catch(err => {
    throw err;
  });

  await new Promise(resolve => setTimeout(resolve, test.entryWaitTime));

  let stepReports: Array<TestRunStepReport> = [];
  let traces: Array<Trace> = [];

  for (const step of test.steps) {
    const trace = await runStep(page, step);
    traces.push(trace);
    const report: TestRunStepReport = await getFlowReport(trace).then(flowReport => ({ flowReport }));
    stepReports.push(report);
  }

  await browser.close();
  return Promise.resolve({
    report: {
      reportName: test.reportName,
      stepReports
    },
    traces
  });
}

async function runStep(page: Page, step: TestRunStep): Promise<Trace> {
  await page.tracing.start({
    path: null,
    screenshots: true
  });

  const button = await page.$(step.actionSelector);
  await button.click();
  await new Promise(resolve => setTimeout(resolve, step.stepWaitTime));

  return page.tracing.stop().then(traceBuffer => JSON.parse(traceBuffer.toString()));
}

export function validateTestRun(test: TestRun): boolean {
  const { reportName, entryUrl, steps } = test;
  // TODO - do better validation
  return !!(reportName && entryUrl && steps);
}
