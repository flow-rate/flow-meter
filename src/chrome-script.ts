const launchChrome = require('@serverless-chrome/lambda');
import * as request from 'superagent';

export const getChrome = async () => {
  const chrome = await launchChrome().catch(err => console.error('error launching chrome', err));
  const response = await request
    .get(`${chrome.url}/json/version`)
    .set('Content-Type', 'application/json').catch(err => console.error('request err', err));

  const endpoint = (response as any).body.webSocketDebuggerUrl;

  return {
    endpoint,
    instance: chrome,
  };
};